# rmw_dds_common

#### 介绍
此存储库包含支持基于 DDS 的 RMW 实现的软件包。

#### 软件架构
软件架构说明

https://github.com/ros2/rmw_dds_common.git

文件内容:
```
rmw_dds_common/
|-- CONTRIBUTING.md
|-- LICENSE
|-- README.md
`-- rmw_dds_common
    |-- CHANGELOG.rst
    |-- CMakeLists.txt
    |-- Doxyfile
    |-- QUALITY_DECLARATION.md
    |-- docs
    |   `-- FEATURES.md
    |-- include
    |   `-- rmw_dds_common
    |       |-- context.hpp
    |       |-- gid_utils.hpp
    |       |-- graph_cache.hpp
    |       `-- visibility_control.h
    |-- msg
    |   |-- Gid.msg
    |   |-- NodeEntitiesInfo.msg
    |   `-- ParticipantEntitiesInfo.msg
    |-- package.xml
    |-- src
    |   |-- gid_utils.cpp
    |   `-- graph_cache.cpp
    `-- test
        |-- allocator_testing_utils.h
        |-- benchmark
        |   `-- benchmark_graph_cache.cpp
        |-- test_gid_utils.cpp
        `-- test_graph_cache.cpp
```
#### 安装教程

1. 下载RPM包

aarch64:

```
wget https://117.78.1.88/build/home:Chenjy3_22.03/openEuler_22.03_LTS_standard_aarch64/aarch64/rmw_dds_common/ros2-foxy-ros2-rmw_dds_common-1.0.3-2.oe2203.aarch64.rpm
```

x86_64:

```
wget https://117.78.1.88/build/home:Chenjy3_22.03/openEuler_22.03_LTS_standard_x86_64/x86_64/rmw_dds_common/ros2-foxy-ros2-rmw_dds_common-1.0.3-2.oe2203.x86_64.rpm
```

2. 安装rpm包

aarch64:

```
sudo rpm -ivh --nodeps --force ros2-foxy-ros2-rmw_dds_common-1.0.3-2.oe2203.aarch64.rpm
```

x86_64:

```
sudo rpm -ivh --nodeps --force ros2-foxy-ros2-rmw_dds_common-1.0.3-2.oe2203.x86_64.rpm
```

#### 使用说明

依赖环境安装:

```
sh /opt/ros/foxy/install_dependence.sh
```

安装完成以后，在/opt/ros/foxy/目录下如下输出,则表示安装成功。

输出:

```
.
├── include
│   └── rmw_dds_common
├── lib64
│   ├── cmake
│   └── librmw_dds_common.so
└── share
    ├── colcon-core
    │   └── packages
    └── rmw_dds_common
        ├── cmake
        ├── environment
        ├── hook
        ├── local_setup.bash
        ├── local_setup.dsv
        ├── local_setup.sh
        ├── local_setup.zsh
        ├── msg
        ├── package.bash
        ├── package.dsv
        ├── package.ps1
        ├── package.sh
        ├── package.xml
        └── package.zsh
```

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
