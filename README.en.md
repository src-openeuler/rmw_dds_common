# rmw_dds_common

#### Description
This repository contains packages to support DDS-based RMW implementations.

#### Software Architecture
Software architecture description

https://github.com/ros2/rmw_dds_common.git

input:
```
rmw_dds_common/
|-- CONTRIBUTING.md
|-- LICENSE
|-- README.md
`-- rmw_dds_common
    |-- CHANGELOG.rst
    |-- CMakeLists.txt
    |-- Doxyfile
    |-- QUALITY_DECLARATION.md
    |-- docs
    |   `-- FEATURES.md
    |-- include
    |   `-- rmw_dds_common
    |       |-- context.hpp
    |       |-- gid_utils.hpp
    |       |-- graph_cache.hpp
    |       `-- visibility_control.h
    |-- msg
    |   |-- Gid.msg
    |   |-- NodeEntitiesInfo.msg
    |   `-- ParticipantEntitiesInfo.msg
    |-- package.xml
    |-- src
    |   |-- gid_utils.cpp
    |   `-- graph_cache.cpp
    `-- test
        |-- allocator_testing_utils.h
        |-- benchmark
        |   `-- benchmark_graph_cache.cpp
        |-- test_gid_utils.cpp
        `-- test_graph_cache.cpp
```
#### Installation

1.  Download RPM

aarch64:

```
wget https://117.78.1.88/build/home:Chenjy3_22.03/openEuler_22.03_LTS_standard_aarch64/aarch64/rmw_dds_common/ros2-foxy-ros2-rmw_dds_common-1.0.3-2.oe2203.aarch64.rpm
```

x86_64:

```
wget https://117.78.1.88/build/home:Chenjy3_22.03/openEuler_22.03_LTS_standard_x86_64/x86_64/rmw_dds_common/ros2-foxy-ros2-rmw_dds_common-1.0.3-2.oe2203.x86_64.rpm
```

2.  Install RPM

aarch64:

```
sudo rpm -ivh --nodeps --force ros2-foxy-ros2-rmw_dds_common-1.0.3-2.oe2203.aarch64.rpm
```

x86_64:

```
sudo rpm -ivh --nodeps --force ros2-foxy-ros2-rmw_dds_common-1.0.3-2.oe2203.x86_64.rpm
```

#### Instructions

Dependence installation:

```
sh /opt/ros/foxy/install_dependence.sh
```

Exit the following output file under the /opt/ros/foxy/ directory,Prove that the software installation is successful.

output:

```
.
├── include
│   └── rmw_dds_common
├── lib64
│   ├── cmake
│   └── librmw_dds_common.so
└── share
    ├── colcon-core
    │   └── packages
    └── rmw_dds_common
        ├── cmake
        ├── environment
        ├── hook
        ├── local_setup.bash
        ├── local_setup.dsv
        ├── local_setup.sh
        ├── local_setup.zsh
        ├── msg
        ├── package.bash
        ├── package.dsv
        ├── package.ps1
        ├── package.sh
        ├── package.xml
        └── package.zsh
```

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
